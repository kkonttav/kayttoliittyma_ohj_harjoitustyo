<?php
include_once 'class/sahkoposti.php';

    $vastaanottaja=filter_input(INPUT_POST,'vastaanottaja',FILTER_SANITIZE_STRING);
    $otsikko=filter_input(INPUT_POST,'otsikko',FILTER_SANITIZE_STRING);
    $viesti=filter_input(INPUT_POST,'viesti',FILTER_SANITIZE_STRING);        
    
    $viesti2 =wordwrap($viesti,70);

    $sposti= new Sahkoposti($vastaanottaja,$otsikko,$viesti2);

    if($sposti->laheta()) {
        print "success";
    }
    else {
        print "error";   
    }
