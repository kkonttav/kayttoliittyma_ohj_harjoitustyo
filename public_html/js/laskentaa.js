$(function() {

    $( "#accordion" ).accordion({
        heightStyle: "content"
    });

    $( "#slider_etaisyys" ).slider({
        min: 0,
        max: 1000,
      slide: function( event, ui ) {
          $("#etaisyys").val( ui.value );
      }    
    });

    $( "#slider_vauhti" ).slider({
        min: 0,
        max: 200,
      slide: function( event, ui ) {
          $("#vauhti").val( ui.value );
      }    
    });
    
    $("#etaisyys").change(function() {
       $("#slider_etaisyys").slider('value',$("#etaisyys").val());
    });
    
    $("#vauhti").change(function() {
       $("#slider_vauhti").slider('value',$("#vauhti").val());
    });

    $('#tunti').timepicker({
        showMinutes: false,
        hourText: 'Tunti',
        showPeriodLabels: false,
        showLeadingZero: false,
        hours: {
            starts: 0,
            ends: 23
        }
    });
    $('#minuutti').timepicker({
        showHours: false,
        minuteText: 'Minuutti',
        rows: 6,
        minutes: {
        starts: 0,                // First displayed minute
        ends: 59,                 // Last displayed minute
        interval: 1              // Interval of displayed minutes
        }
    });
    
    $('#sekunti').timepicker({
        showHours: false,
        minuteText: 'sekunti',
        rows: 6,
        minutes: {
        starts: 0,                // First displayed minute
        ends: 59,                 // Last displayed minute
        interval: 1              // Interval of displayed minutes
        }
    });
    
    $.ajaxSetup({
        cache:false
    });


    /* HUOM!!! Tätä alla olevaa en saanut toimimaan...?
     * Se toimi minulla suoralla formin post kutsulla,
     * mutta halusin tehdä ajax kutsun tuohon jotta saisin UI:n paremmaksi.
     *  
     * Eli tuossa alla on koodi joka olisi sitten lähettänyt annettu tiedot
     * tuolle php:lle joka sitten lähettää varsinaisen sähköpostin.
     *  
     * Ongelmaksi tuossa tuli ettei tuo koskaan mennyt sposti.php:lle.
     * Tuosta myös puuttuu virhe tilanne käsittely jos postin lähetys epäonnistuisi.
     *  
     * */
    $("#sposti").click(function(){
        var vastaanottaja=$("#vastaanottaja").val();
        var otsikko=$("#otsikko").val();
        var viesti=$("#viesti").val();
        
        $.post("sposti.php", {
            vastaanottaja:vastaanottaja,
            otsikko:otsikko,
            viesti:viesti
            }, function(data) {
                    alert("Viesti lähetetty!");
                    }, 'text');
    });

$("#OHki").click(function () {
    tyhjenna();
    $("#etaisyys").val(606);
    $("#slider_etaisyys").slider('value',606);
    $("#vauhti").val(100);
    $("#slider_vauhti").slider('value',100);
    laske();
    $("#GoogleMap").attr("src", "https://mapsengine.google.com/map/embed?mid=zVR9A3az3QI8.kSnwlRLEIOl4");
    });
    
$("#OTam").click(function () {
    tyhjenna();
    $("#etaisyys").val(491);
    $("#slider_etaisyys").slider('value',491);
    $("#vauhti").val(100);
    $("#slider_vauhti").slider('value',100);
    laske();
    $("#GoogleMap").attr("src", "https://www.google.com/maps/d/embed?mid=zVR9A3az3QI8.kpY9R74hrtNM");
    });
    
$("#OTur").click(function () {
    tyhjenna();
    $("#etaisyys").val(647);
    $("#slider_etaisyys").slider('value',647);
    $("#vauhti").val(100);
    $("#slider_vauhti").slider('value',100);
    laske();
    $("#GoogleMap").attr("src", "https://www.google.com/maps/d/embed?mid=zVR9A3az3QI8.kX1kXafhauVA");
    });
    
$("#ORov").click(function () {
    tyhjenna();
    $("#etaisyys").val(221);
    $("#slider_etaisyys").slider('value',221);
    $("#vauhti").val(100);
    $("#slider_vauhti").slider('value',100);
    laske();
    $("#GoogleMap").attr("src", "https://www.google.com/maps/d/embed?mid=zVR9A3az3QI8.kHLsmlSx_CJQ");
    });    

$("#OTuk").click(function () {
    tyhjenna();
    $("#etaisyys").val(1155);
    $("#slider_etaisyys").slider('value',1155);
    $("#vauhti").val(100);
    $("#slider_vauhti").slider('value',100);
    laske();
    $("#GoogleMap").attr("src", "https://www.google.com/maps/d/embed?mid=zVR9A3az3QI8.kQcWVgfsIJRE");
    });     
    
});

function laskeaika(matka, nopeus) {
// 3,6,12,24,48,96,192
    var aika = matka/(nopeus/3600);    //Aika sekunneissa

    if(matka == nopeus) /* Korjataan "ongelmallinen" laskenta */
        {
        aika = Math.round(aika);
        }    
    
    var tuntia   = Math.floor(aika / 3600);
    var minuuttia = Math.floor((aika - (tuntia * 3600)) / 60);
    var sekuntia = Math.round(aika - (tuntia * 3600) - (minuuttia * 60));

    if (tuntia    < 10) {tuntia    = "0"+tuntia;}
    if (minuuttia < 10) {minuuttia = "0"+minuuttia;}
    if (sekuntia  < 10) {sekuntia  = "0"+sekuntia;}
    var matkaaika    = tuntia + 'h:' + minuuttia + 'min:' + sekuntia + "s";
    return matkaaika;    
}

function laskenopeus(matka, aika) {

    var nopeus = matka/aika;    //Aika tunneissa
    
    return nopeus.toFixed(2);
}

function laskematka(nopeus, aika) {

    var matka = nopeus * aika;    //Aika tunneissa
    
    return matka.toFixed(2);
}

function laske() {
    document.getElementById("errorMessage").innerHTML = ""; /* Poista mahdollinen virhe teksti! */
    document.getElementById("tulos").innerHTML = ""; /* Poista mahdollinen edellinen laskenta tulos! */
    var matka = parseFloat(document.getElementById("etaisyys").value);
    var nopeus = parseFloat(document.getElementById("vauhti").value);
    var tunti = parseFloat(document.getElementById("tunti").value);
    var minuutti = parseFloat(document.getElementById("minuutti").value);
    var sekunti = parseFloat(document.getElementById("sekunti").value);
/*        if(isNaN(tunti)) /* Jos tuneja ei annettu nollaa se 
            tunti=0;
        if(isNaN(minuutti)) /* Jos minuutteja ei annettu nollaa se 
            minuutti=0;
*/
        if(isNaN(sekunti)) /* Jos sekunteja ei annettu nollaa se */
            sekunti=0;
    if((!isNaN(matka) && matka !=0) && (!isNaN(nopeus) && nopeus !=0) && ((!isNaN(tunti) && tunti!=0) || (!isNaN(minuutti) && minuutti !=0) || (!isNaN(sekunti) && sekunti !=0) )) { /* matka, nopeus ja aika annettu */
        document.getElementById("errorMessage").innerHTML="Liikaa syötteitä, poista yksi.<p>";
        return false;
    }
    else if(isNaN(matka) && isNaN(nopeus) && (isNaN(tunti) || isNaN(minuutti) || isNaN(sekunti)) ) {
        document.getElementById("errorMessage").innerHTML="Et antanut ollenkaan syötteitä, anna kaksi syötettä!<p>";
    }
    else if((!isNaN(matka) && matka !=0) && (!isNaN(nopeus) && nopeus !=0)) { /* matka ja nopeus on annettu */
        /* LASKE */
        document.getElementById("tulos").innerHTML = "Matkaan käytettävä aika on: " + laskeaika(matka,nopeus);
        $("#GoogleMap").attr("src", "img/auto2.png");
        $("#otsikko").val("Matka-ajan laskentatulos.");
        $("#viesti").val("Tervehdys, Tässä antamiesi arvojen mukainen laskentatulos Matkaan käytetty aika on: " + laskeaika(matka,nopeus) + 
                                                      " Annettu matka:" + matka + "km. Annettu nopeus: " + nopeus + "km/h.");
        }
    else if((!isNaN(matka) && matka !=0) && tarkista_aika(tunti, minuutti, sekunti, true)) { /* matka ja aika annettu */
        /* LASKE */
        var aika = parseFloat(tunti) + parseFloat(minuutti/60) + parseFloat(sekunti/3600);
        document.getElementById("tulos").innerHTML = "Tarvittava nopeus: " + laskenopeus(matka,aika) + "km/h. ";
        $("#GoogleMap").attr("src", "img/auto2.png");
        $("#otsikko").val("Matkanopeuden laskentatulos.");
        $("#viesti").val("Tervehdys, Tässä antamiesi arvojen mukainen laskentatulos Matkaan käytetty nopeus on: " + laskenopeus(matka,aika) + "km/h" +  
                                                      " Annettu matka:" + matka + "km. Annettu aika: " + tunti + "h :" + minuutti + "min :" + sekunti + "s.");
    }
    else if((!isNaN(nopeus) && nopeus !=0) && tarkista_aika(tunti, minuutti, sekunti, true)) { /* nopeus ja aika annettu */
        /* LASKE */
        var aika = parseFloat(tunti) + parseFloat(minuutti/60) + parseFloat(sekunti/3600);
        document.getElementById("tulos").innerHTML = "Kuljettu matka olisi: " + laskematka(nopeus, aika) + "km.";
        $("#GoogleMap").attr("src", "img/auto2.png");
        $("#otsikko").val("Matkaetäisyyden laskentatulos.");
        $("#viesti").val("Tervehdys, Tässä antamiesi arvojen mukainen laskentatulos Matkaetäisyys on: " + laskematka(nopeus, aika) + "km" +
                                                      " Annettu nopeus: " + nopeus + "km. Annettu aika: " + tunti + "h :" + minuutti + "min :" + sekunti + "s.");
    }
    else {
        if(tarkista_aika(tunti, minuutti, sekunti, false)) { /* tarkista_aika tekee omat virhe tekstit!! */
            document.getElementById("errorMessage").innerHTML="Anna vähintään kaksi syötettä!<p>";
            }
    }
}

function tarkista_aika(tunti, minuutti, sekunti, nayta_virhe) {
    if((!isNaN(tunti) && tunti!=0) || (!isNaN(minuutti) && minuutti !=0)) {
        if(tunti >=0) {
            if(minuutti <60 && minuutti >=0) {
                if(sekunti <60 && sekunti >=0) {
                    /* Kaikki aika syötteen on valideja! */
                    return true;
                    }
                else {
                    if(nayta_virhe) {
                        document.getElementById("errorMessage").innerHTML="Sekuntien tulee olla 0-59!<p>";
                        }
                    return false;
                    }
                }
            else {
                if(nayta_virhe) {
                    document.getElementById("errorMessage").innerHTML="Minuutttien tulee olla 0-59!<p>";
                    }
                return false;
                }
            }
        else {
            if(nayta_virhe) {
                document.getElementById("errorMessage").innerHTML="Korjaa antamasi tunnit syöte!<p>";
                }
            return false;
            }
        }
    else {
        if(nayta_virhe) {
            document.getElementById("errorMessage").innerHTML="Anna vähintään kaksi syötettä!<p>";
            }
        return false;
        }
}

function tyhjenna() {
    document.getElementById("etaisyys").value = "";
    document.getElementById("vauhti").value = "";
    document.getElementById("tunti").value = "";
    document.getElementById("minuutti").value = "";
    document.getElementById("sekunti").value = "";
    document.getElementById("errorMessage").innerHTML = "";
    document.getElementById("tulos").innerHTML = "";
    $("#slider_etaisyys").slider('value',0);
    $("#slider_vauhti").slider('value',0);
    $("#GoogleMap").attr("src", "img/auto3.png");
}
