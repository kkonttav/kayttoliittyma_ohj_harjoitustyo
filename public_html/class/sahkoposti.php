<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sahkoposti
 *
 * @author jjuntune
 */
class Sahkoposti {
    private $vastaanottaja="";
    private $aihe="";
    private $viesti="";
    
    function __construct($vastaanottaja,$aihe,$viesti) {
        $this->vastaanottaja=$vastaanottaja;
        $this->aihe=$aihe;
        $this->viesti=$viesti;
    }
    
    public function laheta() {
        $otsake="From: kari@matkalaskuri.net" . "\r\n" .
                "Reply-To: kari@matkalaskuri.net" . "\r\n" . 
                "X-Mailer: PHP/" . phpversion();
        return mail($this->vastaanottaja,$this->aihe,$this->viesti,$otsake);
    }
}
?>
